import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';

class LoginView extends StatefulWidget {
  const LoginView({super.key});

  @override
  State<LoginView> createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  GoogleSignInAccount? _currentUser;
  final GoogleSignIn _googleSignIn = GoogleSignIn(
    scopes: [
      'email',
      'https://www.googleapis.com/auth/contacts.readonly',
    ],
  );

  @override
  void initState() {
    _googleSignIn.onCurrentUserChanged.listen((account) {
      setState(() {
        _currentUser = account;
      });
    });
    _googleSignIn.signInSilently();
    super.initState();
  }

  Future<void> _handleSignIn() async {
    try {
      await _googleSignIn.signIn();
    } catch (error) {
      if (kDebugMode) {
        print(error);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: SingleChildScrollView(
            child: Column(
              children: [
                _currentUser != null
                    ? Container(
                        child: ListTile(
                          leading: GoogleUserCircleAvatar(
                            identity: _currentUser!,
                          ),
                          title: Text(
                            _currentUser!.displayName ?? "",
                          ),
                          subtitle: Text(
                            _currentUser!.email,
                          ),
                          trailing: IconButton(
                            icon: Icon(Icons.logout_outlined),
                            onPressed: () async {
                              await _googleSignIn.disconnect();
                            },
                          ),
                        ),
                      )
                    : ElevatedButton(
                        onPressed: () async {
                          await _handleSignIn();
                        },
                        child: Text('Login Google Sign In'),
                      )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
